package ios;

import static java.lang.System.out;

public class Formats {
	
	static void formatFloat() {
		Float f = 4/(float)7;
		out.printf("%25.10f\n", f);
	}
	
	static void naturals() {
		int[] nats = {5, 7, 11, 13, 17};
		for (int n : nats) {
			long n2 = (long) Math.pow(n, 2);
			long n3 = (long) Math.pow(n, 3);
			
			out.printf("%15d%15d%15d%n", n, n2, n3);
		}
	}
	
	static void roots() {
		for (int i=2;i<=10;i++) {
			out.printf("%d\t%.6f%n", i, Math.pow(5, 1/(float)i));
		}
	}
	
	public static void main(String[] args) {
		formatFloat();
		naturals();
		roots();
	}
}
