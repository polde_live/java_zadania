package basics;

public class uppcase {

	public static void main(String[] args) {
		char[] cs = {'p', 'r', 'o', 'g', 'r', 'a', 'm', 'i', 'n', 'g'};
		
		for (int i=0; i < cs.length; i++) {
			if (i!=0) { 
				System.out.print(cs[i]);
			}
			else {
				System.out.print(Character.toUpperCase(cs[i]));
			}
		}
		System.out.println();
		
		for (char c : cs ) {
			char c2 = Character.toUpperCase(c);
			System.out.print(c2);
		}

	}

}
