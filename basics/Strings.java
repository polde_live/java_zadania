package basics;

import static java.lang.System.out;
import java.lang.StringBuilder;

public class Strings {
	
	public static void hello() {
		String hello = "hello there.";
		out.println(hello.toUpperCase());
		
		boolean first = true;
		for (char c : hello.toCharArray()) {
			
			if (first) {
				out.print(Character.toUpperCase(c));
				first = false;
			}
			else out.print(c);
			}
		out.println();
	}

	public static void reverse() {
		String str = "programing";
		for (int i=str.length()-1; i>=0; i--) {
			out.print(str.charAt(i));
		}
		out.println();
	}
	
	public static void buildDigits() {
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<10; i++) {
			sb.append(Integer.toString(i));
		}
		out.println(sb.toString());
		
	}
	
	public static void main(String[] args) {
		hello();
		reverse();
		buildDigits();
	}
	
}
