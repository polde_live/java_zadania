package basics;

import static java.lang.System.out;

public class Arrays {

	public static void intArray() {
		
		int[] arr = new int[20];
		arr[0] = 123;	
		out.println(arr[0]);
		out.println(arr.length);
		
	}
	
	@SuppressWarnings("unused")
	public static void numericArrays() {
		final int A_SIZE = 5;
		float[] floats = new float[A_SIZE];
		int[] ints = new int[A_SIZE];
		
		Float[] Floats = new Float[A_SIZE];
		Integer[] Integers = new Integer[A_SIZE];
		double[] doubles = new double[A_SIZE];
		boolean[] bools = new boolean[A_SIZE];
	}
	
	public static String[] poem() {
		String[] poem = new String[10];
		poem[0] = "Mary";
		poem[1] = "had";
		poem[2] = "a";
		poem[3] = "little";
		poem[4] = "lamb";
		return poem;
	}
	
	public static void manyInts() {
		int[] intArr = new int[150];
		out.println(intArr.length);
	}
	
	public static void referenceArray() {
		// Assign array to another and change it.
		// Two names are pointing to the same object.
		int[] arrA = {1, 2, 3, 4, 5, 6};
		int[] arrB = arrA;
		arrB[0] = -999;
		out.printf("arrB[0]: %d%n", arrB[0]);
		out.printf("arrA[0]: %d%n", arrA[0]);
	}
	
	public static void copyArray() {
		// copy array to demonstrate that they are different objects
		int[] arrA = {1, 2, 3, 4, 5, 6};
		int[] arrB = java.util.Arrays.copyOf(arrA, arrA.length);
				
		arrB[0] = -999;
		out.printf("arrB[0]: %d%n", arrB[0]);
		out.printf("arrA[0]: %d%n", arrA[0]);
	}
	
	public static void main(String[] args) {

		intArray();
		numericArrays();
		String[] poem = poem();
		out.println(poem.length);
		manyInts();
		referenceArray();
		copyArray();
	}

}
