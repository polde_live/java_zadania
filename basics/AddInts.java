package basics;
import static java.lang.System.out;

public class AddInts {

	public static int addIntegers(int a, int b)  {
		return a+b;
	}
	public static int addIntegers(int... intArr) {
		int sum;
		sum = 0;
		for (int i : intArr) {
			sum += i;
		}
		return sum;
	}
	public static void main(String[] args) {
		int a, b, c;
		
		a = 15;
		b = 40;
		out.printf("%d+%d=%d%n", a, b, addIntegers(a, b));
		
		c = 25;
		out.printf("%d+%d+%d=%d%n", a, b, c, addIntegers(a, b, c));
		
		int[] arr = {100, 24, 32, 41, 56, 67, 78, 89, 90};
		out.printf("result=%d%n", addIntegers(arr));
		
	}

}
